const tasks = [
    {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
    {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
    {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
    {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
    {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
    {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
    {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
];

const frontendTimeSpent =[];
const typeBugTimeSpent = [];
const uiTaskAmount = [];
const moreThan4HourTasksArray = [];



const getAmount = tasks.reduce((acc, obj) =>{
    const cat = obj.category;
    const typeOf = obj.type;
    const time = obj.timeSpent;
    const title = obj.title;
    const oneTask = 1;

    if (cat === "Frontend") {
        frontendTimeSpent.push(time);
        acc.Frontend++;
    }

    if (time > 4) {
        const moreThan4HourTasks = {
            title: title,
            category: cat,
        };
        moreThan4HourTasksArray.push(moreThan4HourTasks)
    }

    if (cat === "Backend") {
        acc.Backend++;
    }

    if (typeOf === "bug") {
        typeBugTimeSpent.push(time)
    }

    if (title.indexOf('UI') !== -1) {
        uiTaskAmount.push(oneTask)
    }
    return acc;
}, {Frontend: 0, Backend: 0});


console.log("Total spent time for front-end tasks: " + frontendTimeSpent.reduce((acc, ob)=> acc + ob));
console.log("Total spent time for bug typed tasks: " + typeBugTimeSpent.reduce((acc, ob)=> acc + ob));
console.log("Total amount of tasks for Title contained UI: " + uiTaskAmount.reduce((acc, ob)=> acc + ob));
console.log("Total amount of tasks for Front-end and Back-end: ", getAmount);
console.log("Array for tasks spent more than 4 hours: ", moreThan4HourTasksArray);
