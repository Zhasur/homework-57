import React from 'react'
import './Spendings.css'

const Spendings = props => {
    return (
        <div className="spending">
        <span className="spending-text">
            {props.text}
        </span>

        <span className="cost">{props.cost} KGS</span>

        <button
            onClick={props.remove}
            className="close-btn"
            id="close"
        >
            Delete
        </button>
        </div>
    )
};

export default Spendings


