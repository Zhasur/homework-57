import React from 'react'

const AddSpendings = props => {
    return (
        <div className="addSpendings-block">
            <input
                className="item-name"
                type="text"
                id={props.inputId}
                placeholder="Add new task"
                onChange={props.change}
                value={props.value}
            />
            <input
                className="cost"
                placeholder="Cost"
                type="text"
                onChange={props.costChange}
            />
            <span className='kgs'>KGS</span>
            <button
                onClick={props.spending}
                className="add-btn"
            >Add
            </button>
        </div>
    )
};

export default AddSpendings

