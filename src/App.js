import React, { Component } from 'react';
import './App.css';
import AddSpendings from './components/AddSpendings'
import Spendings from './components/Spendings'

class App extends Component {
    state = {
        spendings: [],
        TotalSpend: 0,
        value: '',
        cost: '',
    };

    removeSpending = index => {
        const spendings = [...this.state.spendings];
        const indexofArray = spendings[index];

        spendings.splice(index, 1);

        const totalSpend = this.state.TotalSpend;
        const newTotalSpend = totalSpend - parseInt(indexofArray.cost);

        this.setState({spendings, TotalSpend: newTotalSpend});
    };

    changeHandler = (event) => {
        this.setState({
            value: event.target.value,
        })
    };

    costChangeHandler = (event) => {
        this.setState({
            cost: event.target.value,
        })
    };

    addSpending = () => {
        const spendings = [...this.state.spendings];
        const newSpending = {spending: this.state.value, cost: this.state.cost};

        if (newSpending.spending !== '') {
            spendings.push(newSpending);
        } else {
            alert('No spending entered!')
        }
        let summ = spendings.reduce((cost, item) => cost + parseInt(item.cost), 0);

        this.setState({spendings, TotalSpend: summ});
    };

  render() {

      let spendings = this.state.spendings.map((spending, index) => {
          return (
              <Spendings
                  key={index}
                  text={spending.spending}
                  remove={() => this.removeSpending(index)}
                  cost={spending.cost}
              />
          )
      });

    return (
      <div className="App">
          <AddSpendings
              value={this.state.value}
              inputId="task-input"
              change={(event) => this.changeHandler(event)}
              costChange={(event) => this.costChangeHandler(event)}
              spending={this.addSpending}
          />
          {spendings}

          <span className="total-spend">Total spent: {this.state.TotalSpend} KGS</span>
      </div>
    );
  }
}

export default App;
